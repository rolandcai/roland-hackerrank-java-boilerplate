# Roland Hackerrank Java Boilerplate

## Getting started

The Solution.java makes use of BufferedReader and BufferedWriter to speed up I/O for Hackerrank use.

## Adjusting buffer size

The input buffer and output buffer has been set to 1MB. Please adjust the buffer size according to the hackerrank question needs.

## Inputs

In the build.gradle config, the application has been configured to read input from input/input01.txt 

## Running

`./gradlew run`

or

`./gradlew.bat run`

