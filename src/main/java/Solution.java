import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;

public class Solution {
    public static void main(String[] args) throws IOException {
        // 1MB buffer for input and output. Adjust to the question's requirement
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in), 1000000);
        PrintWriter pw =
                new PrintWriter(new BufferedWriter(new OutputStreamWriter(System.out), 1000000));
        String line;
        while ((line = br.readLine()) != null) {
            pw.println(line);
        }

        pw.close(); //remember to close the pw to flush the data
    }
}
